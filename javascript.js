// combinations = checkInitDataPosition(flattenSplit).length;
//flattenSplit = hasNegation(flattenSplit);
//console.log(acumOp)
//console.log(elDuplicates(elNegation(flattenSplit)));

// //method1 fail
// for (let i = 5; i > 0; i--) {
//     if (operations.indexOf(rules[i]) > -1 && !(isArrayInArray(brakets, rules[i]))) {
//         acumOp.push(inputText.split(regRules[i]));
//     }
// }

// function elNegation(arr) { //eliminate negation
//     for (let i = 0; i < arr.length; i++)
//         arr[i] = arr[i].split("¬").pop();
//     return arr;
// }
// function elDuplicates(arr) {
//     let uniqueArray = arr.filter(function (item, pos) {
//         return arr.indexOf(item) == pos;
//     });
//     return uniqueArray;
// }

// function split(arr, n) {
//     if (n <= 1) return [arr];
//     let partLength = Math.ceil(arr.length / n),
//         part1 = arr.slice(0, partLength),
//         result = split(arr.slice(partLength), n - 1);
//     result.unshift(part1);
//     return result;
// }

// function splitTree(arr, m) {
//     if (m.length < 1) return arr;
//     let result = split(arr, m[0]);
//     return result.map(function (sublist) {
//         return splitTree(sublist, m.slice(1));
//     });
// }

let table = document.getElementById("taut");
//let satisTable = document.getElementById("satis");
//let nesatisTable = document.getElementById("nesatis");
//let continTable = document.getElementById("contin");

const neg = new RegExp(/[[¬]+/, "g");
const and = new RegExp(/[[^]+/, "g");
const or = new RegExp(/[[∨]+/, "g");
const xor = new RegExp(/[[⊕]+/, "g");
const impl = new RegExp(/[[→]+/, "g");
const equiv = new RegExp(/[[↔]+/, "g");

let rules = ["¬", "^", "∨", "⊕", "→", "↔"];
let regRules = [neg, and, or, xor, impl, equiv];
let tableLength = 0;
let combinations = 0;
let negArr = [];
let auxSplit = [];
let splitOp = [];
let resultInitFill = [];
//let acumOp = [];
let split = [];
let exRules = [];
let exOp = [];
let exSplit = [];
//let splitted2 = [];
let flattenSplit = [];
let sel = 0;
let toExport = document.getElementById("export");

function insertAtCursor(myField, myValue) {
    //IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = myValue;
    }
    //MOZILLA and others
    else if (myField.selectionStart || myField.selectionStart == "0") {
        let startPos = myField.selectionStart;
        let endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos) +
            myValue +
            myField.value.substring(endPos, myField.value.length);
    } else {
        myField.value += myValue;
    }
}

function resetTables() {
    toExport.innerHTML = "";
    tableLength = 0;
    negArr = [];
    resultInitFill = [];
    combinations = 0;
    //let acumOp = [];
    split = [];
    //let splitted2 = [];
    flattenSplit = [];
    auxSplit = [];
    sel = 0;
    splitOp = [];
    table.innerHTML = "";
    //    nesatisTable.innerHTML = "";
    //  continTable.innerHTML = "";
    //satisTable.innerHTML = "";
}

function isArrayInArray(arrS, item) {
    if (arrS == null) return 0;
    let item_as_string = arrS.toString();

    let contains = item_as_string.includes(item);
    return contains;
}

function makeUnique(str) {
    return String.prototype.concat(...new Set(str));
}

//method2
function returnPush(arr, counter, brakets) {
    let result = [];

    if (counter == 0) return arr;
    if ((isArrayInArray(brakets, rules[counter]))) {
        counter -= 1;
        return arr;
    } else {
        result = arr.split(regRules[counter]);
        splitOp.push(result);
    }
    return result.map((sublist) => {
        return returnPush(sublist, counter - 1, brakets);
    });
}

function checkAvailability(arr, val) {
    return arr.some(arrVal => val == arrVal);
}

// function splitOnOp(arr, operations, brakets) {
//     let result = [];
//     let aux = [];
//     if (brakets != null)
//         for (let i = 0; i < brakets.length; i++) {
//             aux[i] = brakets[i].match(/[⊕^→∨↔]+/g);
//         }
//     for (let i = 1; i < rules.length; i++) {
//         if (checkAvailability(operations, rules[i]) && !(checkAvailability(aux, rules[i])))
//             result.push(arr.split(regRules[i]));
//     }
//     return result;
// }
// function size(ar) {
//     let row_count = ar.length;
//     let row_sizes = [];
//     for (let i = 0; i < row_count; i++) {
//         row_sizes.push(ar[i].length);
//     }
//     return [row_count, Math.min.apply(null, row_sizes)];
// }

function checkRules(operations, brakets) { //find used operation and if brakes were used
    let aux = [];
    if (brakets != null)
        for (let i = 0; i < brakets.length; i++) {
            aux[i] = brakets[i].match(/[⊕^→∨↔]+/g);
        }
    let opMade = [];
    for (let i = 1; i < rules.length; i++) {
        if (checkAvailability(operations, rules[i]) && !(checkAvailability(aux, rules[i]))) opMade.push(rules[i]);
    }
    return opMade;
}


function flatten(array) {
    return array.reduce((memo, el) => {
        let items = Array.isArray(el) ? flatten(el) : [el];
        return memo.concat(items);
    }, []);
}

function hasNegation(arr) {
    //eliminate item that has brakets
    let index = arr.indexOf("(");
    arr.splice(index, 1);
    for (let i = 0; i < arr.length; i++) {
        if (!(arr.includes(arr[i].split("¬").pop()))) arr.push(arr[i].split("¬").pop());
    }
    //console.log(arr);
    return arr;
}

function isArrayInArray2(arr, item) {
    let item_as_string = JSON.stringify(item);

    let contains = arr.some(function (ele) {
        return JSON.stringify(ele) === item_as_string;
    });
    return contains;
}

function elDuplicates(arr) {
    let seen = {};
    return arr.filter(function (item) {
        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });

}


function extractBrakets(arr) {
    let result = [];
    arr.forEach(element => {
        result.push(element.match(/[a-zA-Z]+/g));
    });
    return result;
}

function getNeg(arr) {
    let copyArr = arr;

    for (let i = 0; i < arr.length; i++)
        copyArr[i] = copyArr[i].match(/[¬]\w+/g);
    return copyArr;
}

function arraysEqual(arr1, arr2) {
    if (arr1.length !== arr2.length)
        return false;
    for (let i = arr1.length; i--;) {
        if (arr1[i] !== arr2[i])
            return false;
    }

    return true;
}

/**
 *  description la functieS
 * @param {String} inputText 
 * @param {} operations 
 * @param {*} brakets
 * @returns {Void} 
 */
function splitIntoPieces(inputText, operations, brakets) {

    let splitNext = [];
    let rules = checkRules(operations, brakets);
    split = returnPush(inputText, 5, brakets); //method 2 recursive split
    //splittedNext = splitOnOp(inputText, operations, brakets);
    // console.log(size(splitOp));

    //console.log(splitOp);
    // console.log(rejoinSeparator(splitOp, 5, brakets));
    // splitOp = Array.from(objtostring(splitOp));
    for (let i = 0; i < (splitOp.length - 1); i++)
        if (arraysEqual(splitOp[i], splitOp[i + 1])) {
            splitOp[i] = splitOp[i].slice(i, 1).filter(x => x.length > 0);
            // delete splitOp[i];
        }

    splitOp = splitOp.filter(x => x.length > 0);

    //console.log(splitOp);

    let initOp = "";
    for (let j = 0; j < splitOp[0].length; j++) {
        if (splitOp[0].length == j + 1) initOp += splitOp[0][j];
        else
            initOp += splitOp[0][j] + rules[rules.length - 1];
    }
    splitNext.push(initOp);
    console.log(splitNext);
    for (let i = 0; i < splitOp.length; i++) {
        let limit = splitOp[i].length;
        for (let j = 0; j < limit; j++) {
            splitNext.push(splitOp[i][j]);
        }
    }
    console.log(splitNext);

    flattenSplit = flatten(split);
    negArr = flattenSplit;



    flattenSplit.sort().reverse();
    console.log(split);

    //console.log(madeOperations);
    exOp = checkRules(operations, brakets);
    console.log(checkRules(operations, brakets));
    flattenSplit = hasNegation(flattenSplit);
    console.log(brakets);

    flattenSplit = flattenSplit.concat(brakets);
    //console.log(extractBrakets(brakets));

    //addfrom brakets
    if (brakets != null)
        if (!isArrayInArray2(flattenSplit, extractBrakets(brakets)))
            flattenSplit = flatten(flattenSplit.concat(extractBrakets(brakets)));
    //add neg values
    negArr = getNeg(negArr);
    negArr = negArr.filter(n => n);
    tableLength = negArr.length;

    flattenSplit = makeUnique(flattenSplit.toString()).split("");
    for (let i = 0; i < flattenSplit.length; i++)
        flattenSplit[i] = flattenSplit[i].match(/[a-zA-Z]+/g);
    flattenSplit = flattenSplit.filter(n => n);

    //console.log(flattenSplit);
    combinations = flattenSplit.length;
    //console.log(combinations);


    flattenSplit = elDuplicates(flattenSplit);
    flattenSplit = flattenSplit.concat(brakets);
    flattenSplit = flattenSplit.concat(negArr);
    //console.log(flattenSplit);
    //flattenSplit = elDuplicates(flattenSplit.concat(splitNext));
    flattenSplit.sort().reverse();
    //combinations = flattenSplit.length;
    auxSplit = elDuplicates(flattenSplit.concat(splitNext));
    auxSplit.sort().reverse();
    auxSplit = auxSplit.filter(x => flattenSplit.indexOf(x) == -1);
    //console.log(auxSplit.length);
    //flattenSplit = flattenSplit.concat(auxSplit);
    flattenSplit = flattenSplit.filter(x => x);
    console.log(flattenSplit);
    exSplit = flatten(flattenSplit);

}

function findParts(inputText) {
    let operations = [];
    let elements = [];
    let brakets = [];
    elements = inputText.match(/[a-zA-Z0-9¬]+/g);
    operations = inputText.match(/[⊕^→∨↔]+/g);
    brakets = inputText.match(/\((.*?)\)+/g);
    return {
        elements: elements,
        operations: operations,
        brakets: brakets
    };
}

function checkInitDataPosition(arr) {
    let result = [];
    //console.log(arr);
    for (let i = 0; i < arr.length; i++) {
        if (/^[a-zA-Z]/.test(arr[i])) {
            result.push(i);
        }
    }
    console.log(result);
    return result;
}

function workText() {

    let inputText = document.getElementById("inputText").value;
    console.log(inputText);
    let partsObj = {};
    let range = 0;
    let range2 = 0;
    partsObj = findParts(inputText);
    let braketsLength = (partsObj.brakets == null) ? 0 : partsObj.brakets.length;
    splitIntoPieces(inputText, partsObj.operations, partsObj.brakets);
    range = tableLength + combinations + braketsLength + auxSplit.length;
    range2 = tableLength + combinations + braketsLength;
    // console.log(tableLength);
    // console.log(combinations);
    // console.log(partsObj.brakets.length);
    // console.log(auxSplit.length);
    tableCreateTaut(range, combinations);
    //tableCreateContin(range, combinations);
    //tableCreateSatis(range, combinations);
    //tableCreateNesatis(range, combinations);

    fillRows(flattenSplit);
    flattenSplit.sort().reverse();
    fillInitialData(range, combinations, checkInitDataPosition(flattenSplit));
    fillNegation(flattenSplit, combinations);
    console.log(flattenSplit);
    if (braketsLength)
        fillBrakets(flattenSplit, partsObj.brakets);

    fillEmpty(flattenSplit, range2, range, combinations, partsObj.brakets);
    fillConditions(range, combinations);
}

//creeaza tabelul taut
function tableCreateTaut(operations, combinations) {
    combinations = Math.pow(2, combinations) + 1;
    console.log(combinations);
    console.log(operations);
    for (let i = 0; i < combinations; i++) {
        let tr = table.insertRow();
        for (let j = 0; j < operations; j++) {
            let td = tr.insertCell();
            td.appendChild(document.createTextNode(" "));
        }
    }
    for (let i = 0; i < combinations; i++)
        for (let j = 0; j < operations; j++)
            table.rows[i].cells[j].innerHTML = 0;
}

// function tableCreateSatis(operations, combinations) {
//     combinations = Math.pow(2, combinations) + 1;
//     for (let i = 0; i < combinations; i++) {
//         let tr = satisTable.insertRow();
//         for (let j = 0; j < operations; j++) {
//             let td = tr.insertCell();
//             td.appendChild(document.createTextNode(" "));
//         }
//     }
//     for (let i = 0; i < combinations; i++)
//         for (let j = 0; j < operations; j++)
//             satisTable.rows[i].cells[j].innerHTML = 0;
// }

// function tableCreateNesatis(operations, combinations) {
//     combinations = Math.pow(2, combinations) + 1;
//     for (let i = 0; i < combinations; i++) {
//         let tr = nesatisTable.insertRow();
//         for (let j = 0; j < operations; j++) {
//             let td = tr.insertCell();
//             td.appendChild(document.createTextNode(" "));
//         }
//     }
//     for (let i = 0; i < combinations; i++)
//         for (let j = 0; j < operations; j++)
//             nesatisTable.rows[i].cells[j].innerHTML = 0;
// }

// function tableCreateContin(operations, combinations) {
//     combinations = Math.pow(2, combinations) + 1;
//     for (let i = 0; i < combinations; i++) {
//         let tr = continTable.insertRow();
//         for (let j = 0; j < operations; j++) {
//             let td = tr.insertCell();
//             td.appendChild(document.createTextNode(" "));
//         }
//     }
//     for (let i = 0; i < combinations; i++)
//         for (let j = 0; j < operations; j++)
//             continTable.rows[i].cells[j].innerHTML = 0;

// }
//gogog
function fillRows(dataArr) {
    dataArr = dataArr.concat(auxSplit.sort((a, b) =>
        // ASC  -> a.length - b.length
        // DESC -> b.length - a.length
        a.length - b.length));
    console.log(dataArr);
    console.log(dataArr.length);

    for (let j = 0; j < dataArr.length; j++) {
        table.rows[0].cells[j].innerHTML = dataArr[j];
        //satisTable.rows[0].cells[j].innerHTML = dataArr[j];
        //nesatisTable.rows[0].cells[j].innerHTML = dataArr[j];
        //continTable.rows[0].cells[j].innerHTML = dataArr[j];
    }
}

function combinationsArr(n) {
    let r = [];
    for (let i = 0; i < (1 << n); i++) {
        let c = [];
        for (let j = 0; j < n; j++) {
            c.push(i & (1 << j) ? "T" : "F");
        }
        r.push(c.join(" "));
    }
    // return r;
    let array = r.toString().split(",");
    for (let i = 0; i < array.length; i++)
        array[i] = array[i].split(" ");
    // console.log(array);
    return array;
}



function fillInitialData(operations, combinations, positionsArr) {
    console.log(positionsArr + "!!");
    combinations = Math.pow(2, combinations);
    resultInitFill = combinationsArr(positionsArr.length);
    console.log(combinations);

    for (let i = 1; i <= combinations; i++)
        for (let j = positionsArr[0], k = 0; j <= positionsArr.length, k < positionsArr.length; j++, k++) {
            table.rows[i].cells[j].innerHTML = resultInitFill[i - 1][k];
            //  satisTable.rows[i].cells[j].innerHTML = resultInitFill[i - 1][k];
            //nesatisTable.rows[i].cells[j].innerHTML = resultInitFill[i - 1][k];
            //continTable.rows[i].cells[j].innerHTML = resultInitFill[i - 1][k];
        }

}

function fillNegation(dataArr, combinations) {
    combinations = Math.pow(2, combinations);
    let resultNeg = [];
    let mapObj = {
        T: "F",
        F: "T"
    };
    let index = [];
    let indexNeg = [];
    let copyNeg = negArr;
    let copyFlatenSplit = flattenSplit;
    copyNeg = flatten(negArr);
    copyFlatenSplit = flatten(flattenSplit);
    copyNeg = copyNeg.filter(n => n);
    copyFlatenSplit = copyFlatenSplit.filter(n => n);

    for (let i = 0; i < copyNeg.length; i++) {
        indexNeg[i] = (copyFlatenSplit.indexOf(copyNeg[i].split("¬").pop())) - copyNeg.length;
    }
    for (let j = 0; j < combinations; j++) {
        index = [];
        for (let i = 0; i < copyNeg.length; i++) {
            index.push(resultInitFill[j][indexNeg[i]].replace(/T|F/gi, (matched) => mapObj[matched]));
        }
        resultNeg.push(index);
    }
    for (let i = 1; i <= combinations; i++) {
        for (let j = 0; j < copyNeg.length; j++) {
            table.rows[i].cells[j].innerHTML = resultNeg[i - 1][j];
            //satisTable.rows[i].cells[j].innerHTML = resultNeg[i - 1][j];
            //nesatisTable.rows[i].cells[j].innerHTML = resultNeg[i - 1][j];
            //continTable.rows[i].cells[j].innerHTML = resultNeg[i - 1][j];
        }
    }
    // resultNeg = resultInitFill
    //     .map((arr) => arr.slice(0, copyNeg.length).map(b => b === "T" ? "F" : "T"));
    // console.log(resultNeg);
}

function fillBrakets(arr, brakets) {
    let braketsLength = (brakets.length == null) ? 0 : brakets.length;
    let pos1 = tableLength + combinations;
    let pos2 = pos1 + braketsLength - 1;
    console.log(pos1 + "!!");
    console.log(pos2 + "!!");
    let operator = 0;
    let elements = [];
    flattenSplit = flatten(flattenSplit);
    let copyBrakets = brakets;
    copyBrakets.reverse();
    console.log(brakets);
    combinations = Math.pow(2, combinations);
    for (let i = 1; i <= combinations; i++) {
        for (let j = pos1, k = 0; j < pos2, k < copyBrakets.length; j++, k++) {
            operator = copyBrakets[k].match(/[⊕^→∨↔]+/g);
            elements = copyBrakets[k].match(/[a-z]+/g);
            //elements = elements[0].split(/[⊕^→∨↔]+/g);
            // console.log(elements);
            // console.log(operator);

            if (operator == "⊕") solveXor(elements, operator, i, j);
            if (operator == "^") solveAnd(elements, operator, i, j);
            if (operator == "→") solveImpl(elements, operator, i, j);
            if (operator == "∨") solveOr(elements, operator, i, j);
            if (operator == "↔") solveEqui(elements, operator, i, j);
        }
    }

}
// for (let i = 0; i < copyBrakets.length; i++) {
//     operator[i] = copyBrakets[i].match(/[⊕^→∨↔]+/g);
// }

function solveXor(elements, operator, row, col) {
    let indexElements = [];
    let copyFlat = flattenSplit;
    copyFlat = copyFlat.concat(auxSplit.sort((a, b) =>
        // ASC  -> a.length - b.length
        // DESC -> b.length - a.length
        a.length - b.length));
    //console.log(elements);
    for (let i = 0; i < elements.length; i++)
        if (copyFlat.indexOf(elements[i]) >= 0) {
            // console.log(flattenSplit.indexOf(elements[i]));
            indexElements.push(copyFlat.indexOf(elements[i]));
        }
    //console.log(col);

    let el1 = (table.rows[row].cells[indexElements[0]].innerHTML == "F") ? 0 : 1;
    let el2 = (table.rows[row].cells[indexElements[1]].innerHTML == "F") ? 0 : 1;
    //console.log(el1);
    //console.log(el2);
    let result = ((el1 == 0 && el2 == 0) || (el1 == 1 && el2 == 1)) ? 0 : 1;
    result = (result) ? "T" : "F";
    table.rows[row].cells[col].innerHTML = result;
    //satisTable.rows[row].cells[col].innerHTML = result;
    //nesatisTable.rows[row].cells[col].innerHTML = result;
    //continTable.rows[row].cells[col].innerHTML = result;
}

function solveAnd(elements, operator, row, col) {
    let indexElements = [];
    let copyFlat = flattenSplit;
    copyFlat = copyFlat.concat(auxSplit.sort((a, b) =>
        // ASC  -> a.length - b.length
        // DESC -> b.length - a.length
        a.length - b.length));
    //console.log(elements);
    for (let i = 0; i < elements.length; i++)
        if (copyFlat.indexOf(elements[i]) >= 0) {
            //   console.log(flattenSplit.indexOf(elements[i]));
            indexElements.push(copyFlat.indexOf(elements[i]));
        }
    //console.log(col);

    let el1 = (table.rows[row].cells[indexElements[0]].innerHTML == "F") ? 0 : 1;
    let el2 = (table.rows[row].cells[indexElements[1]].innerHTML == "F") ? 0 : 1;
    //console.log(el1);
    //console.log(el2);
    let result = ((el1 == 1 && el2 == 1)) ? 1 : 0;
    result = (result) ? "T" : "F";
    table.rows[row].cells[col].innerHTML = result;
    //satisTable.rows[row].cells[col].innerHTML = result;
    //nesatisTable.rows[row].cells[col].innerHTML = result;
    //continTable.rows[row].cells[col].innerHTML = result;
}

function solveImpl(elements, operator, row, col) {
    let indexElements = [];
    let copyFlat = flattenSplit;
    copyFlat = copyFlat.concat(auxSplit.sort((a, b) =>
        // ASC  -> a.length - b.length
        // DESC -> b.length - a.length
        a.length - b.length));
    console.log(elements);
    for (let i = 0; i < elements.length; i++)
        if (copyFlat.indexOf(elements[i]) >= 0) {
            //console.log(flattenSplit.indexOf(elements[i]));
            indexElements.push(copyFlat.indexOf(elements[i]));
        }
    console.log(indexElements);
    //console.log(table.rows[1].cells[indexElements[0]].innerHTML);
    let el1 = (table.rows[row].cells[indexElements[0]].innerHTML == "F") ? 0 : 1;
    let el2 = (table.rows[row].cells[indexElements[1]].innerHTML == "T") ? 1 : 0;
    //console.log(el1);
    //console.log(el2);
    let result = ((el1 == 1 && el2 == 0) || (el2 == 1 && el1 == 0)) ? 0 : 1;
    result = (result) ? "T" : "F";
    table.rows[row].cells[col].innerHTML = result;
    //satisTable.rows[row].cells[col].innerHTML = result;
    //nesatisTable.rows[row].cells[col].innerHTML = result;
    //continTable.rows[row].cells[col].innerHTML = result;
}

function solveOr(elements, operator, row, col) {
    let indexElements = [];
    let copyFlat = flatten(flattenSplit);
    copyFlat = copyFlat.concat(auxSplit.sort((a, b) =>
        // ASC  -> a.length - b.length
        // DESC -> b.length - a.length
        a.length - b.length));
    // console.log(elements);
    for (let i = 0; i < elements.length; i++)
        if (copyFlat.indexOf(elements[i]) >= 0) {
            //alert(copyFlat.indexOf(elements[i]));
            indexElements.push(copyFlat.indexOf(elements[i]));
        }
    //console.log(table.rows[row].cells[indexElements[0]].innerHTML);
    let el1 = (table.rows[row].cells[indexElements[0]].innerHTML == "F") ? 0 : 1;
    let el2 = (table.rows[row].cells[indexElements[1]].innerHTML == "T") ? 1 : 0;
    // console.log(el1);
    //console.log(el2);
    let result = (el1 == 0 && el2 == 0) ? 0 : 1;
    result = (result) ? "T" : "F";
    table.rows[row].cells[col].innerHTML = result;
    //satisTable.rows[row].cells[col].innerHTML = result;
    //nesatisTable.rows[row].cells[col].innerHTML = result;
    //continTable.rows[row].cells[col].innerHTML = result;
    indexElements = [];
}

function solveEqui(elements, operator, row, col) {
    let indexElements = [];
    let copyFlat = flattenSplit;
    copyFlat = copyFlat.concat(auxSplit.sort((a, b) =>
        // ASC  -> a.length - b.length
        // DESC -> b.length - a.length
        a.length - b.length));
    // console.log(elements);
    for (let i = 0; i < elements.length; i++)
        if (copyFlat.indexOf(elements[i]) >= 0) {
            //console.log(flattenSplit.indexOf(elements[i]));
            indexElements.push(copyFlat.indexOf(elements[i]));
        }
    //console.log(row);
    let el1 = (table.rows[row].cells[indexElements[0]].innerHTML == "F") ? 0 : 1;
    let el2 = (table.rows[row].cells[indexElements[1]].innerHTML == "T") ? 1 : 0;
    //console.log(el1);
    //console.log(el2);
    let result = ((el1 == 0 && el2 == 0) || (el1 == 0 && el2 == 0)) ? 1 : 0;
    result = (result) ? "T" : "F";
    table.rows[row].cells[col].innerHTML = result;
    //satisTable.rows[row].cells[col].innerHTML = result;
    //nesatisTable.rows[row].cells[col].innerHTML = result;
    //continTable.rows[row].cells[col].innerHTML = result;
}

function fillEmpty(arr, pos1, operations, combinations2, brakets) {
    //let braketsLength = (brakets.length == null) ? 0 : brakets.length;
    let elements = [];
    let operator = "";
    let operatorCheck = "";
    let checkTable = 0;
    let copySplit = arr;
    // let op = operations;
    copySplit = flatten(copySplit);
    copySplit = copySplit.concat(auxSplit.sort((a, b) =>
        // ASC  -> a.length - b.length
        // DESC -> b.length - a.length
        a.length - b.length));
    //combinations2 = Math.pow(2, combinations2);
    let difference = exSplit.filter(x => brakets.indexOf(x) == -1);
    console.log(pos1);
    console.log(operations);
    console.log(difference);
    console.log(brakets);
    console.log(auxSplit);
    console.log(exOp);
    let check = 0;
    //element should be i-1 and j-1
    for (let k = 1; k <= combinations; k++) {
        for (let i = 0; i < auxSplit.length; i++) {
            for (let x = 0; x < brakets.length; x++) {
                if (auxSplit[i].includes(brakets[x])) {
                    elements.push(auxSplit[i].match(/[(¬a-z⊕^→∨↔)]+/g));
                    //alert(auxSplit[i].match(/[(¬a-z)]+/g));
                    elements = flatten(elements)[0].split(exOp[i]);
                } else {
                    for (let j = 0; j < difference.length; j++) {
                        if ((auxSplit[i].match(/[¬a-z]+/g)).includes(difference[j])) {
                            elements.push(auxSplit[i].split(exOp[i]));
                            //alert(copySplit[i].split(exOp[i]));
                        }
                    }
                    elements = elDuplicates(flatten(elements));
                }
            }
            //elements = elements
            operator = exOp[i];
            console.log(elements);
            console.log(operator);
            if (operator == "⊕") solveXor(elements, operator, k, i + pos1);
            if (operator == "^") solveAnd(elements, operator, k, i + pos1);
            if (operator == "→") solveImpl(elements, operator, k, i + pos1);
            if (operator == "∨") solveOr(elements, operator, k, i + pos1);
            if (operator == "↔") solveEqui(elements, operator, k, i + pos1);
            elements = [];
            operator = "";

        }
    }
}

function fillConditions(operations, combinations) {
    let taut = [];
    let satis = [];
    let nesatis = [];
    let contin = [];
    let result = "";
    console.log(operations);
    console.log(table.rows[0].cells[operations - 1].innerHTML);
    for (let i = 1; i <= combinations; i++) {
        for (let j = 0; j < operations; j++) {
            taut.push(table.rows[i].cells[operations - 1].innerHTML);
            satis.push(table.rows[i].cells[operations - 1].innerHTML);
            nesatis.push(table.rows[i].cells[operations - 1].innerHTML);
            contin.push(table.rows[i].cells[operations - 1].innerHTML);
        }
    }
    if (taut.every((x) => x == "T")) result += " Tautologie ";
    if (satis.some((x) => x == "T")) result += " Satisfiabila ";
    if (nesatis.every((x) => x == "F")) result += " Nesatisfiabila ";
    if (contin.some((x) => x == "T" || x == "F")) result += " Contingenta ";
    toExport.innerHTML += `<h1>${result}</h1>`;
}